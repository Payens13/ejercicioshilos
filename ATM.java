import java.util.Random;
import java.util.concurrent.Semaphore;

public class ATM extends Thread {
    private String name;
    private final Semaphore cajeroDispo; //me lo pide intellij

    public ATM(String name, Semaphore cajeroDispo) {
        this.name = name;
        this.cajeroDispo = cajeroDispo;
    }


    @Override
    public void run() {
        int processingTime = (int) (Math.random()*10+1);

        for (int i = 1; i <= processingTime; i++) {
            System.out.println("ATM " + name + " está en uso (" + i + " segundos)");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("ATM " + name + " ha terminado de procesar.");
    }
}
