import java.util.concurrent.Semaphore;

public class ATMMain {
    public static void main(String[] args) {
        // Crear el semáforo con 3 permisos (3 cajeros disponibles)
        Semaphore cajeroDispo = new Semaphore(3);

        // Lanzar 5 cajeros
        ATM atm1 = new ATM("Cajero1", cajeroDispo);
        ATM atm2 = new ATM("Cajero2", cajeroDispo);
        ATM atm3 = new ATM("Cajero3", cajeroDispo);
        ATM atm4 = new ATM("Cajero4", cajeroDispo);
        ATM atm5 = new ATM("Cajero5", cajeroDispo);

        atm1.start();
        atm2.start();
        atm3.start();
        atm4.start();
        atm5.start();

        // Crear cajas
        Caja caja1 = new Caja("Caja1");
        Caja caja2 = new Caja("Caja2");
        Caja caja3 = new Caja("Caja3");

        // Ejemplo de procesamiento de compra
        caja1.procesarCompra("ClienteA", 3);
        caja2.procesarCompra("ClienteB", 2);
        caja3.procesarCompra("ClienteC", 4);
    }
}