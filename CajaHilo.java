import java.util.Random;
import java.util.concurrent.Semaphore;

class CajaHilo extends Thread {
    private String nombre;
    private Semaphore cajeroDispo;

    public CajaHilo(String nombre, Semaphore cajeroDispo) {
        this.nombre = nombre;
        this.cajeroDispo = cajeroDispo;
    }

    @Override
    public void run() {
        int numProductos = (int) (Math.random()*5+1);

        try {
            cajeroDispo.acquire(); // Adquirir un permiso del semáforo
            System.out.println("Caja " + nombre + " atendiendo al cliente con " + numProductos + " productos");
            Thread.sleep(numProductos * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            cajeroDispo.release(); // Liberar el permiso al finalizar
            System.out.println("Caja " + nombre + " ha terminado de atender al cliente");
        }
    }
}