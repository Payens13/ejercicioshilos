import java.util.Random;

public class Caja {
    private String nombre;
    public Caja(String nombre) {
        this.nombre = nombre;
    }
    public void procesarCompra(String cliente, int productos) {
        System.out.println("La caja " + nombre + " recibe al cliente " + cliente + ".");

        Random random = new Random();
        for (int i = 1; i <= productos; i++) {
            int tiempoProcesamiento = (int) (Math.random()*10+1);
            System.out.println("Caja " + nombre + " - Cliente: " + cliente + " - Producto " + i +
                    " (tiempo de procesamiento: " + tiempoProcesamiento + " segundos)");
            try {
                Thread.sleep(tiempoProcesamiento * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Caja " + nombre + " ha terminado de procesar la compra del cliente " + cliente + ".");
    }
}


