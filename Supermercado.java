import java.util.concurrent.Semaphore;

public class Supermercado {
    public static void main(String[] args) {
        // Crear el semáforo con 2 permisos (2 clientes a la vez)
        Semaphore cajeroDispo = new Semaphore(2);

        // Crear 3 cajas
        CajaHilo caja1 = new CajaHilo("Caja1", cajeroDispo);
        CajaHilo caja2 = new CajaHilo("Caja2", cajeroDispo);
        CajaHilo caja3 = new CajaHilo("Caja3", cajeroDispo);

        // Iniciar las cajas
        caja1.start();
        caja2.start();
        caja3.start();
    }
}